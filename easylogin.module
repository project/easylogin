<?php

/**
 * @file
 * Easy login - allows users to log in to site from an URL </shudders>.
 */

// todo - easylogin url not created, when user is later given the permission to easylogin
// todo - easylogin url is created, even if the permission is remvoed from easylogin role on update


/**
 * HOOKS *********************************************************
 */
 
 /**
 * Implementation of hook_help()
 */
function easylogin_help($path, $arg) {
  if ($path == 'admin/help#easylogin') {
    return t('Easylogin is a module for small sites where the end-users do not, or, are not able to use the normal login process, and you want them to be able to log in via an url. You must grant the user/s the permission to log in via a URL. Also, if so wished, you may give them the permission to be able to reset their own URL.');
  }
}

/**
 * Implementation of hook_menu()
 * CALLBACK to handle login url
 */
function easylogin_menu() {
  $items = array();
  $items['easylogin/%'] = array(
    'title' => 'Easy Login',
    'access callback' => TRUE,
    'page arguments' => array(1),
    'page callback' => '_easylogin_login',
    'type' => MENU_CALLBACK,
  );
  
  // LOCAL TASK CALLBACK for user page
  $items['user/%user/easylogin'] = array(
    'title' => 'Easy Login',
    'access callback' => '_easylogin_access_profile',
    'access arguments' => array(1),
    'page callback' => '_easylogin_userdata',
    'page arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implementation of hook_perm()
 */
function easylogin_perm(){
  return array(
    'login from url',
    'reset own url',
  );
}

/**
 * Implementation of hook_user().
 */
function easylogin_user($op, &$edit, &$account, $category = NULL){
  switch ($op){
    // new user added...
    case 'insert':
      include_once drupal_get_path('module', 'easylogin') . '/easylogin_admin.inc';
      $url = easylogin_hash($account->uid);
      db_query("INSERT INTO {easylogin_urls} (uid, url) VALUES (%d, '%s')", $account->uid, $url);
      
      // only show this message to users who can administer users to not bother new users
      global $user;
      if (user_access('administer users')) {
        drupal_set_message(t('Easylogin URL for user @username has been created.', array('@username' => $account->name)));
      }
      break;

    // if user is changed from  active to blocked or vice versa...
    case 'validate':
      if ($account->uid) {
      
        // user has gone from active to blocked - compare old status to new status
        if ($edit['_account']->status == 1 && $edit['status'] == 0 && db_result(db_query("SELECT 1 FROM {easylogin_urls} WHERE uid = %d", $account->uid))) {
          db_query("DELETE FROM {easylogin_urls} WHERE uid = %d", $account->uid);
          drupal_set_message(t('Easylogin URL for user @username has been removed.', array('@username' => $account->name)));
        }
        
        //  user has gone from blocked to active
        else if ($edit['_account']->status == 0 && $edit['status'] == 1 && !db_result(db_query("SELECT 1 FROM {easylogin_urls} WHERE uid = %d", $account->uid))) {
          include_once drupal_get_path('module', 'easylogin') . '/easylogin_admin.inc';
          $url = easylogin_hash($account->uid);
          db_query("INSERT INTO {easylogin_urls} (uid, url) VALUES (%d, '%s')", $account->uid, $url);
          drupal_set_message(t('A new Easylogin URL for user @username has been created.', array('@username' => $account->name)));
        }
      }
      break;
      

    // user removed
    case 'delete':
      db_query("DELETE FROM {easylogin_urls} WHERE uid = %d", $account->uid);
      drupal_set_message(t('Easylogin URL for user @username has been removed.', array('@username' => $account->name)));
      break;
  
    // add easylogin url to user object if exists
    case 'load':
      $easylogin_url = db_result(db_query("SELECT url FROM {easylogin_urls} WHERE uid = %d", $account->uid));
      if ($easylogin_url){
        global $base_url;

        // clean url support
        if (variable_get('clean_url', FALSE)) {
          $account->easylogin_url = $base_url . '/easylogin/' . $url;  
        }
        else {
          $account->easylogin_url = $base_url . '/?q=easylogin/' . $url;
        }
      }
      break;
  }
}

/**
 * Implementation of hook_mail().
 */
function easylogin_mail($key, &$message, $params){
  $language = $message['language'];
  $variables = user_mail_tokens($params['account'], $language);
  $variables['!easylogin_fullurl'] = $params['easylogin_fullurl'];

  switch($key){
    case 'your_url':
      $message['subject'] = t('Log in URL from !site', $variables, $language->language);
      $message['body'] = t("Dear !username\n\nYour login URL is:\n!easylogin_fullurl", $variables, $language->language);
      break;
      
    case 'changed_url':
      $message['subject'] = t('New log in URL from !site', $variables, $language->language);
      $message['body'] = t("Dear !username\n\nYour login URL has been reset and is:\n!easylogin_fullurl\n\nThanks", $variables, $language->language);
      break;      
  }
}

/**
 * CALLBACKS *************************************************************************************
 */
 
 /**
 * _easylogin_login
 * check url data, and handle as appropriate
 */
function _easylogin_login($code){
  // if login destination module is installed, then set _GET['destination'] to ensure redirect
  if (module_exists('login_destination')){
    $_GET['destination'] = $_GET['q'];
  }

  $resultuid = db_result(db_query("SELECT uid FROM {easylogin_urls} WHERE url = '%s'", $code));

  if ($resultuid && ($account = user_load($resultuid)) && user_access('login from url', $account)) {
    user_external_login($account);    
    drupal_goto($_REQUEST['destination']);
    exit;
  }
  else{
    drupal_not_found();
    exit;
  }
}

 /**
 * access check that current user can access login link on login page
 */
function _easylogin_access_profile($account){
  global $user;

if (
      $account->uid != 1 && $account->status == TRUE && user_access('login from url', $account) && (
        ( user_access('administer users') && $account->uid != $user->uid)
        || $user->uid == 1
      )
    ) {
    return TRUE;
  }
}

/**
 * _easylogin_userdata
 * show relevant data to user in local task in user account pages
 *
 */
function _easylogin_userdata($account){
  drupal_set_title('Easy Login');

  $url = db_result(db_query("SELECT url FROM {easylogin_urls} WHERE uid = %d", $account->uid));
  
  global $base_url;
  
  // if clean urls are enabled display clean url, otherwise show non-clean (dirty)  url to user
  // nb. if clean urls are later enabled, the user will still be able to use the 'old' dirty url
  if (variable_get('clean_url', FALSE)) {
    $fullurl = $base_url . '/easylogin/' . $url;  
  }
  else {
    $fullurl = $base_url . '/?q=easylogin/' . $url;
  }

  $content = '<p>' . t('This is your private URL which allows you to quickly and easily log in without entering your details into a login box. Please keep this URL safe and secure.') . '</p>';
 
  $form = array();
  $form['loginurl'] = array(
    '#type' => 'item',
    '#title' => t('My easylogin URL'),
    '#value' => $fullurl,
  );
  $content .= drupal_render_form('easylogin-login-url', $form);
//  $content .= '<p>' . t('Your Login URL is: !fullurl', array('!fullurl' => $fullurl)) . '</p>';

  if (user_access('reset own url') || user_access('administer users')){
    $content .= drupal_get_form('_easylogin_userform', $uid, $url);  
  }

  return $content;
}


/**
 * FORM HANDLING *******************************************************************
 */ 
 
 /**
 * _easylogin_userform
 * build form for user to reset url
 */
function _easylogin_userform($form_state, $uid, $url){
  $form = array();
  $form['#validate'][] = '_easylogin_userform_validate';
  $form['#submit'][] = '_easylogin_userform_submit';

  $form['easylogin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Reset URL'),
    '#description' => t('Reset the url to another randomly generated url by pressing the %reset_button_text button. This will email you with a new copy of the link for your safekeeping.', array('%reset_button_text' => t('Reset URL'))),
  );
  $form['easylogin']['sendemail'] = array(
    '#type' => 'checkbox',
    '#title' => t('Email new URL'),
    '#default_value' => FALSE,
  );
  $form['easylogin']['reseturl'] = array(
    '#type' => 'submit',
    '#value' => 'Reset URL',
  );
  return $form;
}

function _easylogin_userform_validate(&$form, $form_state){
  $url = db_result(db_query("SELECT url FROM {easylogin_urls} WHERE uid = %d", $form['#parameters'][2]));
  
  if ($url != $form['#parameters'][3]){
    return FALSE;
  }  
  return TRUE;
}

function _easylogin_userform_submit(&$form, $form_state){
  include_once drupal_get_path('module', 'easylogin') . '/easylogin_admin.inc';

  $url = easylogin_hash($form['#parameters'][2]);
  db_query("UPDATE {easylogin_urls} SET url = '%s' WHERE uid = %d", $url, $form['#parameters'][2]);
  drupal_set_message(t('Your login URL has been reset.'));

  if ($form_state['values']['sendemail']) {
    // get email address of current user
    $account = user_load(arg(1));
    drupal_mail('easylogin', 'changed_url', $account->mail, user_preferred_language($account), array('account' => $account, 'easylogin_fullurl' => $account->easylogin_url));
    drupal_set_message(t('Your new login URL has been emailed to you.'));
  }
}
